import requests
import json

# 存储模拟运动数据
url = 'https://api-all-sporter.megacombine.com/api/test/save/play/data'
headers = {
    "Content-Type":"application/json",
    'token': 'fdc9b5062cb52ba1fb0a03a4d5b7d23d'
}

files = {"bluetooth_data": ["0x55061000003f870000618200006e72aa"], "user_play_id": 34027514317770752}

data = json.dumps(files)



print(data)
play = requests.post(url, data=data, headers=headers)
# 接口返回的状态码
print(play.status_code)
# 接口返回的json格式内容
dt3 = play.json()
print(dt3)