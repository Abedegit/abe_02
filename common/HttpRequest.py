import requests
from common.LogTools import DoLogs
import json
import threading

class Request:
    log = DoLogs(__name__)

    def __init__(self):
        self.session = requests.sessions.session()

    def http_request(self, method, url, data=None,files=None,headers=None, json=None,cookies=None,timeout=10):
        global resp
        self.log.mylog.debug("测试地址是{}".format(url))
        if json is None:
            self.log.mylog.debug("测试参数是{}".format(data))
        else:
            self.log.mylog.debug("测试参数是{}".format(json))
        if type(data) == str:
            data = eval(data)
        headers = eval(headers)
        method = method.upper()
        if method == "GET":
            resp = self.session.request(method, url, params=data, headers=headers, cookies=cookies)
        elif method == "POST":
            if json:
                print("json")
                resp = self.session.request(method, url, json=json, headers=headers, cookies=cookies)
            elif files:
                resp = requests.post(url, headers=headers, data=data, files=files, timeout=float(timeout))
            else:
                resp = self.session.request(method, url, data=data, headers=headers, cookies=cookies)
        else:
            print("暂时不支持的格式")
        self.log.mylog.debug("响应信息是{}".format(resp))
        return resp

    def close(self):
        self.session.close()


if __name__ == '__main__':
    res = Request()
    url = 'http://wmoaapi.doocom.net/api/v1/bumph/saveBumph/'
    params1 = {"userId": "65658"}
    headers1 = '{"Content-Type": "application/x-www-form-urlencoded",' \
               '"Authorization":"A57295237F927D06036D6339B5164D0C"}'
    test = res.http_request("post", url, data=params1, headers=headers1)
    print(type(test), test.text)
