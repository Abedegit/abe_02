# -*- coding:utf-8 -*-
from common import context
from common import constant
from common.ExcelTools import DoExcel
import jsonpath
import pytest,json
from requests_toolbelt import MultipartEncoder

# pytestmark = pytest.mark.slow

@pytest.mark.usefixtures("common_instance")
class TestMemo:
    excel = DoExcel(constant.excel_dir, "repair_Parametrization2")
    cases = excel.read_excel()

    @pytest.mark.parametrize("case", cases)
    def test1_memo(self, common_instance, case):
        """
        添加公文
        :param case:
        :return:
        """
        file_path = constant.file_path_jpg
        common_instance[3].mylog.info("当前执行的用例是:{}".format(case.title))
        if "#" in case.url:
            case_url = context.param_replace(case.url)
        else:
            case_url = case.url

        #先查看header头里面有没有#号，有的话就要去正则替换，没有的话就直接取值
        if "#" in case.headers:
            case_headers = context.param_replace(case.headers)
        else:
            case_headers = case.headers
        #先查看data是不是空的，如果是空的直接取值，如果不是的话就正则替换，其他两个附件的单独拿出来出来，最后没有#号的直接取值
        if case.data == None:
            case_data = case.data
        else:
            if "#" in case.data and case.title != "附件上传" and case.title != "附件上传_完成报修":
                case_data = context.param_replace(case.data)
                case_data = case_data.encode()
                print("case_data type is : %s" % type(case_data))
                print("case_data  %s" % case_data)
            elif case.title == "附件上传":
                case_data = {'relateType': (None, 'REPAIR'), 'fileData': ('1.jpg', open(file_path, 'rb'))}
            elif case.title == "附件上传_完成报修":
                case_data = {'relateType': (None, 'REPAIR_COMPLETE'), 'fileData': ('1.jpg', open(file_path, 'rb'))}
            elif case.data != None:
                case_data = case.data
                case_data = case_data.encode()
                print("case_data %s" % case_data)
            else:
                case_data = case.data
                print("case_data %s" % case_data)
        url = common_instance[0].get_value("dev_info", "oa_domain_name") + case_url
        print("url is :%s" % url)
        print("case_headers is %s" % case_headers)

        #附件拿到不同的id存到类反射里面，非附件需要存类反射的单独判断
        if case.title == "附件上传":
            res = common_instance[1].http_request(case.method, url, data={}, files=case_data, headers=case_headers)
            print("响应报文为 %s" % res.text)
            REPAIR_attachment_Id = res.json()["model"]["id"]
            setattr(context.Context, "REPAIR_attachment_Id", str(REPAIR_attachment_Id))
        elif case.title == "附件上传_完成报修":
            res = common_instance[1].http_request(case.method, url, data={}, files=case_data, headers=case_headers)
            print("响应报文为 %s" % res.text)
            REPAIRS_attId = res.json()["model"]["id"]
            setattr(context.Context, "REPAIRS_attId", str(REPAIRS_attId))
        else:
            res = common_instance[1].http_request(case.method, url, data=case_data, headers=case_headers)
            print("响应报文为 %s" % res.text)
            if case.title == "新增项目":
                itemid = res.json()["model"]["id"]
                setattr(context.Context, "itemid", str(itemid))
            if case.title == "添加报修":
                repairs_id = res.json()["model"]["id"]
                setattr(context.Context, "repairs_id", str(repairs_id))
            if case.title == "新增审批组":
                groupid = res.json()["model"]["id"]
                setattr(context.Context, "groupid", str(groupid))
            if case.title == "重新申请报修":
                itemid = res.json()["model"]["id"]
                setattr(context.Context, "itemid", str(itemid))

        global result
        try:
            print("这里的类型为： %s" % type(case.expected))
            print("这里的类型为： %s" % res.json()["success"])
            assert case.expected == res.json()["success"]
            result = "pass"
        except AssertionError as e:
            result = "fail"
            raise e
        finally:
            # self.excel.write_excel(case.case_id, res.status_code, result)
            common_instance[3].mylog.info("当前执行的结果是:{}".format(result))
